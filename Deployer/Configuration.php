<?php

/**
 * @version 0.1
 */

namespace Trehinos;

use Symfony\Component\Yaml\Yaml;

class Configuration implements ConfigInterface
{

    private $config;
    const BASEDIR = __DIR__ . '/..';

    const BASE_CONFIG = [
        'deploy' => [
            'name' => null, # string
            'description' => null, # string
            'url' => null, # string
            'target' => null, # string
            'envs' => [ # array

                # arrays of string of bash commands to launch

                'all' => null, # array of string
                'dev' => null, # array of string
                'debug' => null, # array of string
                'prod' => null, # array of string
            ]
        ]
    ];

    public function __construct(string $projectName, array $config = [])
    {
        $config = array_filter($config);

        $yml = Yaml::parseFile(self::BASEDIR . '/projects/' . strtolower($projectName) . '.yml');

        $this->config = array_merge(
            self::BASE_CONFIG['deploy'],
            $yml['deploy'],
            $config
        );
    }

    public function getProjectName(): ?string
    {
        return $this->config['name'];
    }

    public function getSourceUrl(): ?string
    {
        return $this->config['url'];
    }

    public function getTargetPath(): ?string
    {
        return $this->config['target'];
    }


}