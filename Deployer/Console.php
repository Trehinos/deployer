<?php

/**
 * @version 1.1
 */

namespace Trehinos;

class Console
{

    /** COLOR SEQUENCE */
    const COLOR_START = "\033[";
    const COLOR_END = 'm';

    /** CURSOR/COLORS STATES */
    const RESET = 0;
    const BRIGHT = 1;
    const DIM = 2;
    const UNDERSCORE = 3;
    const BLINK = 5;
    const REVERSE = 7;
    const HIDDEN = 8;

    /** COLORS */
    const FOREGROUND = 30;
    const BACKGROUND = 40;
    const BLACK = 0;
    const RED = 1;
    const GREEN = 2;
    const YELLOW = 3;
    const BLUE = 4;
    const MAGENTA = 5;
    const CYAN = 6;
    const LIGHT_GRAY = 7;

    /** CLEAR SCREEN SEQUENCE */
    const CLEAR = "\033[H\033[J";

    /** CURSOR CONTROL SEQUENCES */
    const CURSOR_HOME = "\033[y,xH";    # unused
    const CURSOR_UP = "\033[yA";        # unused
    const CURSOR_DOWN = "\033[yB";      # unused
    const CURSOR_RIGHT = "\033[xC";     # unused
    const CURSOR_LEFT = "\033[xD";      # unused
    const CURSOR_POS = "\033[y;xf";
    const CURSOR_SAVE = "\033[s";       # unused
    const CURSOR_UNSAVE = "\033[u";     # unused
    const CURSOR_SAVEALL = "\0337";     # unused
    const CURSOR_RESTORE = "\0338";     # unused

    private $argv;

    public function __construct(?array $l_argv = null)
    {
        global $argv;

        $this->argv = $l_argv ?? $argv;
    }

    /**
     * Reset the cursor
     */
    public function __destruct()
    {
        $this->color(self::RESET);
    }

    /**
     * Change Foreground and Background colors.
     *
     * @param array $colors [ STATE [,FOREGROUND + COLOR] [,BACKGROUND + COLOR] ]
     */
    public function color(int $state, int $fc = self::LIGHT_GRAY, int $bc = self::BLACK)
    {
        $fcolor = self::FOREGROUND + $fc;
        $bcolor = self::BACKGROUND + $bc;

        echo self::COLOR_START . "$state;$fcolor;$bcolor" . self::COLOR_END;
    }

    /**
     * Locate the cursor in the specified row ($y) and col ($x).
     *
     * @param int $y
     * @param int $x
     */
    public function locate(int $y, int $x)
    {
        echo str_replace(['x', 'y'], [$x, $y], self::CURSOR_POS);
    }

    /**
     * Clear the screen
     */
    public function clear()
    {
        echo self::CLEAR;
    }

    /**
     * Print a text on the screen.
     *
     * @param string $text
     */
    public function write(string $text)
    {
        echo $text;
    }

    /**
     * Print a text on the screen. Then, print a new line.
     *
     * @param string $text
     */
    public function writeln(string $text)
    {
        echo "$text\n";
    }

    public function option(string $o, string $default = null)
    {
        $arg_index = array_search("-$o", $this->argv);

        if (false !== $arg_index) {
            return $this->argv[++$arg_index ] ?? $default;
        }

        return $default;
    }

}
