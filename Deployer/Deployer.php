<?php

/**
 * @version 0.1
 */

namespace Trehinos;

class Deployer
{

    const VERSION = '0.1-dev';

    private $verbose;
    private $console;
    private $configuration;

    public function __construct(Console $c, ConfigInterface $ci, int $v = 0)
    {
        $this->verbose = $v;
        $this->console = $c;
        $this->configuration = $ci;
    }

    public function c_out(string $out, int $level = 0)
    {
        if ($level <= $this->verbose) {
            switch ($level) {
                case 0:
                    $this->console->color(Console::RESET, Console::LIGHT_GRAY);
                    break;
                case 1:
                    $this->console->color(Console::RESET, Console::GREEN);
                    break;
                case 2:
                    $this->console->color(Console::RESET, Console::YELLOW);
                    break;
                case 3:
                    $this->console->color(Console::RESET, Console::RED);
                    break;
            }

            $this->console->writeln($out);

            $this->console->color(Console::RESET, Console::LIGHT_GRAY);
        }
    }

    public function deploy(string $env = 'dev')
    {
        $this->c_out('TREHINOS DEPLOYER ' . self::VERSION . "\n\n");

        $name = $this->configuration->getProjectName();
        $url = $this->configuration->getSourceUrl();
        $path = $this->configuration->getTargetPath();

        if (!('' !== $name && '' !== $url && '' !== $path)) {
            $this->c_out('ERROR : name, url and path are required.', 3);
        }
        $this->c_out("Deploy project : {$name} : ${url}");
    }

}

