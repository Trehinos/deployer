<?php

/**
 * @version 0.1
 */

namespace Trehinos;

interface ConfigInterface
{

    public function getProjectName(): ?string;

    public function getSourceUrl(): ?string;

    public function getTargetPath(): ?string;

}