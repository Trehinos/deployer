#!/usr/bin/php
<?php

require_once 'vendor/autoload.php';

use Trehinos\Deployer;
use Trehinos\Configuration;
use Trehinos\Console;

$console = new Console();

$deployer = new Deployer(
    $console,
    new Configuration('test', [
        'name' => $console->option('name'),
        'url' => $console->option('url'),
        'target' => $console->option('target')
    ])
);

$deployer->deploy();
