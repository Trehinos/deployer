# Trehinos Deployer 0.1

## Description
Deploiement de projets GIT sur serveur PHP.

## Usage 
deployer projectName [-env=dev]

### -env
* dev : Mode de développement.
* debug : Mode de débogage.
* prod : Mode de production.